import datetime

TEMPLATE = \
"""
<html>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        <style type='text/css'>
            body {font-family: helvetica;}
            h2.header {font-size: 14pt;}
            table {font-size: 10pt; width: 100%;}
            td {border: 1px solid silver; }

            td.dayHeader {text-align: center; font-weight: bold; }
            td.currentDayHeader {text-align: center; font-weight: bold; background-color: gray; color: white;}
            td.personName {text-align: right; padding-right: 10px; font-weight: bold; }
            td.status {text-align: center;}
            td.currentDayStatus {text-align: center; border: 2px solid gray;}
            
            td.statusW {background-color: #EEEE88;}
            td.statusH {background-color: #DD6666;}
            td.statusR {background-color: #ADD8E6;}
            td.statusV {background-color: #66CC66;}
            td.statusX {background-color: #808080;}
            td.statusP {background-color: #FFB0BB;}
            td.statusU {background-color: #FF8F50;}
            td.statusO {background-color: #FFE4C4;}
            td.statusZ {background-color: #9955BB;}
            td.statusL {background-color: #7766CC;}
            td.statusQ {background-color: #333333;}
            td.statusL {background-color: #7766CC;}
            td.statusL {background-color: #7766CC;}
            
            table.legend {margin-top: 30px; width: 100%;}
            table.legend td.status {width: 25px;}
            table.legend td.description {border: none; padding-left: 5px; padding-right: 20px; test-align: left;
            
        </style>
    </head>
    
    <body>
        <h2 class="header">Vacation leaves from DATE_FROM to DATE_TO</h2>
        <table class="data">
            <tr>
                <th></th>
            
DAY_HEADERS

            </tr>

PERSON_ROWS

        </table>
        <table class="legend">
            <tr>
                <td class="status statusH">&nbsp;</td><td class="description">Public holiday</td>
                <td class="status statusR">R</td><td class="description">Requested vacation</td>
                <td class="status statusV">V</td><td class="description">Vacation</td>
                <td class="status statusX">X</td><td class="description">Urgent day-off</td>
                <td class="status statusP">P</td><td class="description">Parental leave</td>
            </tr><tr>
                <td class="status statusU">U</td><td class="description">Unpaid vacation</td>
                <td class="status statusO">O</td><td class="description">Compassionate leave</td>
                <td class="status statusZ">Z</td><td class="description">Working day on public holiday/weekend</td>
                <td class="status statusL">L</td><td class="description">Day in lieu</td>
                <td class="status statusQ">Q</td><td class="description">Employee has quit</td>
            </tr>
        </table>

	<!-- version:GENERATION_STAMP:noisrev -->

    </body>
</html>
"""

DAY_HEADER_TEMPLATE = "<td class='DAYTYPE dayHeader'>DATE</td>"
PERSON_ROW_TEMPLATE = "<tr><td class='personName'>PERSON</td>STATUS_CELLS</tr>"
STATUS_CELL_TEMPLATE = "<td class='DAYTYPE statusSTATUS'>LABEL</td>"

def statusLabel(status):
    return status if status not in ('W', 'H', '') else "&nbsp;"

def dayToDate(day):
    today = datetime.datetime.today()
    date = datetime.date.fromordinal(datetime.date(today.year, 1, 1).toordinal() + day)
    return date.isoformat()

def dayToMonthDay(day):
    today = datetime.datetime.today()
    date = datetime.date.fromordinal(datetime.date(today.year, 1, 1).toordinal() + day)
    return ("0" if date.day < 10 else "")+str(date.day)

def html(fromDay, currentDay, toDay, persons):
    result = TEMPLATE
    result = result.replace("DATE_FROM", dayToDate(fromDay))
    result = result.replace("DATE_TO", dayToDate(toDay))
    dayHeaders = []
    for day in range(fromDay, toDay + 1):
        header = DAY_HEADER_TEMPLATE
        header = header.replace("DAYTYPE", "currentDayHeader" if currentDay == day else "dayHeader")
        header = header.replace("DATE", dayToMonthDay(day))
        dayHeaders.append(header)
    result = result.replace("DAY_HEADERS", "\n".join(dayHeaders))
    personRows = []
    for person in persons:
        row = PERSON_ROW_TEMPLATE.replace("PERSON", person['name'])
        statusCells = []
        for day in range(fromDay, toDay+1):
            status = person['statuses'].get(day, "")
            statusCell = STATUS_CELL_TEMPLATE
            statusCell = statusCell.replace("DAYTYPE", "currentDayStatus" if day == currentDay else "status")
            statusCell = statusCell.replace("STATUS", status)
            statusCell = statusCell.replace("LABEL", statusLabel(status))
            statusCells.append(statusCell)
        row = row.replace("STATUS_CELLS", "".join(statusCells))
        personRows.append(row)
    result = result.replace("PERSON_ROWS", "\n".join(personRows))
    result = result.replace("GENERATION_STAMP", datetime.datetime.now().strftime("%d-%m-%Y--%H-%M"))
    
    return result
