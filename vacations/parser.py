import xlsx
REFERENCE_CELL = "Employee"
DAYS_BEFORE = 7
DAYS_AFTER= 28

"""
calculate range of displayed days (0-based)
"""
def viewRange():
    import datetime
    today = datetime.datetime.now()
    DAYS_IN_YEAR = daysInYear(today.year)
    day = today.toordinal() - datetime.date(today.year, 1, 1).toordinal()
    if day - DAYS_BEFORE < 0:
        return (0, day, DAYS_BEFORE + DAYS_AFTER)
    elif day + DAYS_AFTER > DAYS_IN_YEAR:
        return (DAYS_IN_YEAR - DAYS_BEFORE - DAYS_AFTER - 1, day, DAYS_IN_YEAR-1)
    else:
        return (day - DAYS_BEFORE, day, day + DAYS_AFTER)

def daysInYear(year):
    year = int(year)
    result = 365
    if year % 4 == 0:
        result += 1
    if year % 100 == 0:
        result -= 1
    if year % 400 == 0:
        result += 1
    return result

"""
returns (fromDay, currentDay, toDay, persons)
"""
def parse(fileName):
    workbook = xlsx.Workbook(fileName)
    sheet = workbook['Main']
    
    
    # find ref column/row
    refCol = None
    refRow = None
    maxRow = 55
    for cells in sheet.rows().itervalues():
        for cell in cells:   
            if cell.value == REFERENCE_CELL:
                refCol = cell.column
                refRow = cell.row
                break
        if refCol is not None and refRow is not None:
            break
    
    # find date for 1st Jan
    startDate = None
    for cell in sheet.rows().get(refRow):
        if cell.value.isdigit():
            startDate = int(cell.value)
            break
    
    # find date columns
    (fromDay, currentDay, toDay) = viewRange()
    days = range(fromDay, toDay + 1)
    print ("START DATE", startDate)
    dateToDay = dict(zip([str(startDate + day) for day in days], days))
    print ("DtD", dateToDay)
    colToDay = {}
    for cell in sheet.rows().get(refRow):
        if cell.value in dateToDay.keys():
            colToDay[cell.column] = dateToDay[cell.value]
    
    # locate persons
    persons = []
    for index in range(refRow+1,maxRow):
        if sheet.rows().get(index):
         for cell in sheet.rows().get(index):
            if cell.column == refCol:
                if len(cell.value) > 0 and cell.value[1]!=' ':
                    persons.append({'row': cell.row,
                                 'name': cell.value
                                 })
                break
    
    for person in persons:
        person['statuses'] = {}
        for cell in sheet.rows().get(person['row']):
            if cell.column in colToDay.keys():
                person['statuses'][colToDay[cell.column]] = cell.value.upper()
    print ("PERSONS", persons)
    
    return (fromDay, currentDay, toDay, persons)

print viewRange()
