import parser
import formatter
import sys

def generate(inputFile, outputFile):
    (fromDay, currentDay, toDay, persons) = parser.parse(inputFile)
    html = formatter.html(fromDay, currentDay, toDay, persons)
    f = open(outputFile, 'w')
    f.write(html.encode('utf-8'))
    f.close()

generate(sys.argv[1], sys.argv[2])